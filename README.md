RVerboice
=========

Access the [Verboice](http://verboice.instedd.org) API from [R](http://www.r-project.org/).

Installation
------------

Checkout the source code:

    git clone https://bitbucket.org/instedd/rverboice.git

Then in the command line:

    cd rverboice
    R CMD build src
    R

Then (once inside R):

    install.packages("RCurl")
    install.packages("rjson")
    install.packages("RVerboice_0.3.tar.gz", repos = NULL, type="source")

Usage
-----

    library("RVerboice")

    # Create a Verboice object to access the API.
    #
    # To point to http://verboice.instedd.org:
    #
    #    Verboice(account, password)
    #
    # To point to another Verboice server:
    #
    #    Verboice(account, password, url="...")
    #
    verboice <- Verboice("john@doe.com", "myPassword123")

    # Enqueue a call:
    queued_call <- verboice$call(channel="mychannel", address="0111553953956")

    # To get the call id:
    call_id <- queued_call$call_id

    # To get the state of the call:
    state <- queued_call$state

    # Enqueue another call, this time with a specific flow:
    queued_call <- verboice$call(channel="mychannel", address="0111553953957", call_flow="myflow")

    # Find a contact by address in a project with id 42:
    contact <- verboice$findContactByAddress(42, "1550323456")
    if (contact) {
        # Get then age variable
        age <- contact$vars$age
    }

    # Get all contacts in a project with id 42:
    contacts <- verboice$contacts(42)
    numContacts <- length(contacts)
    firstContact <- contacts[[1]]

    # Get all contacts' addresses in a project with id 42:
    addresses <- verboice$contactsAddresses(42)

    # Update a contact vars in a project with id 42:
    contact <- verboice$updateContactVarsByAddress(42, "1550323456", age=38)

    # Update all contacts vars in a project with id 42:
    contacts <- verboice$updateContactsVars(42, age=35)

Documentation
-------------

To read this documentation from inside R:

    help(Verboice, RVerboice)
